React
---

**Вводный курс по React'у**
Раздел Quick Start
* https://reactjs.org/docs/hello-world.html

React Native
---

**Вводный курс по React Native**
Раздел The Basics
* https://facebook.github.io/react-native/docs/getting-started.html


TypeScript
---

**Конфигурация TypeScript в проекте**
* https://medium.com/@rintoj/react-native-with-typescript-40355a90a5d7

**Вводный курс**
* https://metanit.com/web/typescript/1.1.php

**Документация**
* https://www.typescriptlang.org

Архитектура
---

**Redux**
*https://redux.js.org/introduction*

**Redux-Observable**
* https://redux-observable.js.org

**Доклад от Netflix**
* https://www.youtube.com/watch?v=AslncyG8whg